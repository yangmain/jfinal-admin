package com.supyuan.component.base.config;

import com.jfinal.component.annotation.AutoBindModels;
import com.jfinal.component.annotation.AutoBindRoutes;
import com.jfinal.component.interceptor.ExceptionInterceptor;
import com.jfinal.component.interceptor.SessionAttrInterceptor;
import com.jfinal.config.*;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.log.Log4jLogFactory;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.dialect.PostgreSqlDialect;
import com.jfinal.plugin.hikaricp.HikariCpPlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.supyuan.component.beelt.BeetlStrUtils;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.supyuan.component.beelt.BeeltFunctions;
import com.supyuan.component.beelt.XSSDefenseFormat;
import com.supyuan.component.handler.BasePathHandler;
import com.supyuan.component.interceptor.AuthInterceptor;
import com.supyuan.component.interceptor.CommonInterceptor;
import com.supyuan.component.interceptor.UserInterceptor;
import com.supyuan.component.interceptor.UserKeyInterceptor;
import com.supyuan.component.util.Config;
import com.supyuan.component.util.JFlyFoxCache;
import com.supyuan.component.handler.HtmlHandler;
import com.supyuan.component.util.cache.Cache;
import com.supyuan.component.util.cache.CacheManager;
import com.supyuan.component.util.cache.ICacheManager;
import com.supyuan.component.util.cache.RedisCache;
import com.supyuan.component.util.cache.impl.MemoryCache;
import com.supyuan.component.util.cache.impl.MemorySerializeCache;
import com.supyuan.component.util.cache.serializable.FSTSerializer;
import com.supyuan.component.util.cache.serializable.SerializerManage;
import com.supyuan.modules.system.job.JobFactoryServiceImpl;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal.JFinalBeetlRenderFactory;

/**
 * API引导式配置
 */
public class BaseConfig extends JFinalConfig {

    /**
     * 配置常量
     */
    public void configConstant(Constants me) {
        //静态页面配置
        me.setDevMode(isDevMode());
        // 设置视图类型为Jsp，否则默认为FreeMarker
        me.setViewType(ViewType.JSP);
        me.setLogFactory(new Log4jLogFactory());
        me.setError401View(Config.getStr("PAGES.401"));
        me.setError403View(Config.getStr("PAGES.403"));
        me.setError404View(Config.getStr("PAGES.404"));
        me.setError500View(Config.getStr("PAGES.500"));
        // 开启日志
        SqlReporter.setLog(true);
        //集成Beetl
        String root = PathKit.getWebRootPath();
        JFinalBeetlRenderFactory rf = new JFinalBeetlRenderFactory();
        rf.configFilePath(root);
        me.setRenderFactory(rf);
        // 获取GroupTemplate ,可以设置共享变量等操作
        GroupTemplate groupTemplate = rf.groupTemplate;
        groupTemplate.registerFunctionPackage("strutil", BeetlStrUtils.class);
        groupTemplate.registerFunctionPackage("flyfox", BeeltFunctions.class);
        groupTemplate.registerFormat("xss", new XSSDefenseFormat());

    }

    /**
     * 配置路由
     */
    public void configRoute(Routes me) {
        me.setBaseViewPath("/pages");
        // 自动绑定
        // 1.如果没用加入注解，必须以Controller结尾,自动截取前半部分为key
        // 2.加入ControllerBind的 获取 key
        me.add(new AutoBindRoutes());
    }

    @Override
    public void configHandler(Handlers me) {
        me.add(new HtmlHandler());
        // 全路径获取
        me.add(new BasePathHandler(Config.getStr("PATH.BASE_PATH")));
    }

    /**
     * 配置模板
     */
    public void configEngine(Engine engine) {

    }

    /**
     * 配置全局拦截器
     */
    public void configInterceptor(Interceptors me) {
        // 异常拦截器，跳转到500页面
        me.add(new ExceptionInterceptor());
        // session model转换
        me.add(new SessionInViewInterceptor());
        // 设置session属性
        me.add(new SessionAttrInterceptor());
        //Spring拦截 已不需要
        //me.add(new IocInterceptor());
        // 用户Key设置
        me.add(new UserKeyInterceptor());
        // 用户认证
        me.add(new UserInterceptor());
        // 菜单认证
        me.add(new AuthInterceptor());
        // 公共属性
        me.add(new CommonInterceptor());
    }

    @Override
    public void configPlugin(Plugins me) {
        //Spring插件 已不需要
        //me.add(new SpringPlugin("classpath*:spring/applicationContext.xml"));
        String db_type = Config.getStr("db_type") + ".";
        String jdbcUrl = Config.getStr(db_type + "jdbcUrl");
        String user = Config.getStr(db_type + "user");
        String password = Config.getStr(db_type + "password");
        String driverClass = Config.getStr(db_type + "driverClass");

        // 配置数据库连接池插件
        HikariCpPlugin druidPlugin = new HikariCpPlugin(jdbcUrl, user, password, driverClass);
        /*druidPlugin.setInitialSize(5);
        druidPlugin.setMinIdle(5);
        druidPlugin.setMaxActive(20);
        druidPlugin.setMaxWait(60000);
        druidPlugin.setTimeBetweenEvictionRunsMillis(60000);
        druidPlugin.setValidationQuery("SELECT 'x'");
        druidPlugin.setTestWhileIdle(true);
        druidPlugin.setTestOnBorrow(false);
        druidPlugin.setTestOnReturn(false);
        druidPlugin.setMaxPoolPreparedStatementPerConnectionSize(20);
        druidPlugin.setFilters("wall,stat");*/
        //druidPlugin.start();
        me.add(druidPlugin);
        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        me.add(arp);
        if (isDevMode()) {
            arp.setShowSql(true);
        }

        // 数据库类型
        if (db_type.startsWith("postgre")) {
            arp.setDialect(new PostgreSqlDialect());
        } else if (db_type.startsWith("oracle")) {
            arp.setDialect(new OracleDialect());
            arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
        } else if (db_type.startsWith("mysql")) {
            arp.setDialect(new MysqlDialect());
        }

        new AutoBindModels(arp);
    }

    /**
     * 初始化
     */
    @Override
    public void onStart() {
        // 初始化Cache为fst序列化
        SerializerManage.add("fst", new FSTSerializer());

        // 设置序列化工具
        SerializerManage.setDefaultKey("java");

        // 设置缓存
        CacheManager.setCache(new ICacheManager() {

            public Cache getCache() {
                String cacheName = "MemorySerializeCache";
                if ("MemorySerializeCache".equals(cacheName)) {
                    return new MemorySerializeCache();
                } else if ("MemoryCache".equals(cacheName)) {
                    return new MemoryCache();
                } else if ("RedisCache".equals(cacheName)) {
                    return new RedisCache();
                } else {
                    throw new RuntimeException("####init cache error!");
                }
            }
        });

        LogKit.info("---------加载启用的job到任务池---------");
        JobFactoryServiceImpl factoryService = new JobFactoryServiceImpl();
        //加载job
        factoryService.addJobList();

        JFlyFoxCache.init();
        LogKit.info("##################################");
        LogKit.info("############系统启动完成##########");
        LogKit.info("##################################");
    }

    @Override
    public void onStop() {
        // 关闭模板
        new JFinalBeetlRenderFactory().groupTemplate.close();
        LogKit.info("##################################");
        LogKit.info("############系统停止完成##########");
        LogKit.info("##################################");
    }

    private boolean isDevMode() {
        return Config.getToBoolean("CONSTANTS.DEV_MODE");
    }

}
