package com.supyuan.component.beelt;

import cn.hutool.core.util.StrUtil;
import com.jfinal.template.TemplateFunctions;
import com.supyuan.modules.system.dict.DictCache;
import com.supyuan.modules.system.user.SysUser;
import com.supyuan.modules.system.user.UserCache;
import com.supyuan.component.util.DateUtils;
import com.supyuan.component.util.NumberUtils;
import com.supyuan.component.util.extend.HtmlUtils;

import java.util.Date;

public class BeeltFunctions extends TemplateFunctions {


	// //////////////////////////数据字典///////////////////////////////////////////

	public static String dictSelect(String type, int selected_value) {
		return DictCache.getSelect(type, selected_value);
	}

	public static String dictSelect(String type, String selected_value) {
		return dictSelect(type, NumberUtils.parseInt(selected_value));
	}

	public static String dictValue(int key) {
		return DictCache.getValue(key);
	}

	public static String dictValue(String key) {
		return dictValue(NumberUtils.parseInt(key));
	}

	public static String dictCode(int key) {
		return DictCache.getCode(key);
	}

	public static String dictCode(String key) {
		return dictCode(NumberUtils.parseInt(key));
	}


	/**
	 * 字符串截取
	 *
	 * 2015年5月25日 下午3:58:45 flyfox 369191470@qq.com
	 *
	 * @param str
	 * @param start
	 * @param end
	 * @return
	 */
	public static String substr(String str, int start, int end) {
		return str == null ? null : str.substring(start, end);
	}

	public static String getNow() {
		return DateUtils.getNow();
	}

	public static String getNow(String regex) {
		return DateUtils.getNow(regex);
	}

	/**
	 * 如果str的长度超过了length,取前length位然后拼上...
	 *
	 * @author yimian
	 * @since 2009.06.11
	 * @param str
	 * @param length
	 * @return
	 */
	public static String suojin(String str, int length) {
		return suojin(str, length, "…");
	}

	/**
	 * 如果str的长度超过了c则取c-sub.length长度,然后拼上sub结尾
	 *
	 * @author wangp
	 * @since 2009.06.11
	 * @param str
	 * @param c
	 * @param sub
	 * @return
	 */
	public static String suojin(String str, int c, String sub) {
		if (isEmpty(str))
			return str;
		if (str.length() <= c)
			return str;
		sub = nvl(sub);
		c = c - sub.length();
		c = c > str.length() ? 0 : c;
		str = str.substring(0, c);
		return str + sub;
	}

	/**
	 * 过滤 ;当instr==null时返回长度为0的""; <br>
	 * 与 nvl(...)系的区别在于只处理null ,不处理长度为0的"";
	 *
	 * @param instr
	 * @return
	 */
	public static String nvl(String instr) {
		return nvl(instr, "");
	}

	/**
	 * 过滤 ,把null和长度为0的""当成同一种情况处理; <br>
	 * 当instr==null||"".equals(instr)时返回defaultValue ;其它情况返回 instr
	 *
	 * @param instr
	 * @param defaultValue
	 * @return
	 */
	public static String nvl(String instr, String defaultValue) {
		return instr == null || "".equals(instr) ? defaultValue : instr;
	}



	/**
	 * split
	 *
	 * 2015年5月17日 下午11:03:39 flyfox 369191470@qq.com
	 *
	 * @param str
	 * @param split
	 * @return
	 */
	public static String[] split(String str, String split) {
		if (StrUtil.isEmpty(str)) {
			return null;
		}
		return str.split(split);
	}

	/**
	 * html预览
	 *
	 * 2015年2月2日 下午3:40:34 flyfox 369191470@qq.com
	 *
	 * @param htmlStr
	 * @return
	 */
	public static String showHTML(String htmlStr, int num, String endStr) {
		String tmpStr = HtmlUtils.delHTMLTag(htmlStr);
		tmpStr = suojin(tmpStr, num + endStr.length(), endStr);
		return tmpStr;
	}

	/**
	 * 获取用户
	 *
	 * 2015年2月26日 下午4:24:39 flyfox 369191470@qq.com
	 *
	 * @param pid
	 * @return
	 */
	public static SysUser getUser(Integer pid) {
		SysUser user = UserCache.getUser(pid);
		return user;
	}

	/**
	 * 获取用户名
	 *
	 * 2015年2月26日 下午4:24:39 flyfox 369191470@qq.com
	 *
	 * @param pid
	 * @return
	 */
	public static String getUserName(Integer pid) {
		SysUser user = UserCache.getUser(pid);
		if (user == null) {
			return "";
		}
		if (StrUtil.isNotEmpty(user.getStr("realname"))) {
			return user.getStr("realname");
		}
		return user.getStr("username");
	}

	/**
	 * 判断date距当前时间是否相差before天
	 *
	 * 2015年5月11日 下午2:07:40 flyfox 369191470@qq.com
	 *
	 * @param date
	 * @param before
	 * @return
	 */
	public static boolean isNew(String date, int before) {
		DateUtils.parseByAll(date).getTime();
		Date d1 = new Date();
		Date d2 = DateUtils.parse(date, DateUtils.DEFAULT_REGEX_YYYY_MM_DD_HH_MIN_SS);
		long diff = d1.getTime() - d2.getTime();
		long days = diff / (1000 * 60 * 60 * 24);
		return days - 7 <= 0;
	}

	public static String timestampToDate(Long date) {

		return timestampToDate(date, DateUtils.DEFAULT_REGEX_YYYY_MM_DD_HH_MIN_SS);
	}

	public static String timestampToDate(Long date,String format) {

		String return_date = "";
		if( date!=null && !"".equals(date) )
			return_date = DateUtils.timestampToDate(date, format);

		return return_date;
	}
	/**
	 * 为空
	 *
	 * 2013年9月8日 下午10:08:44
	 *
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str == null || "".equals(str);
	}

	/**
	 * 不为空
	 *
	 * 2013年9月8日 下午10:08:31
	 *
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static String subStringShowInHtml(String str, int length) {
		if (isEmpty(str) || length < 0) {
			return "";
		}

		if (length >= str.length()) {
			return str;
		}

		return str.substring(0, length) + "...";
	}

}
